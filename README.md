# blocker

Automatically blocks Twitter users whose biographies contain certain keywords.

The tool will search all followers of a user, check their biographies, and block them if their bio contains any of the provided keywords.

You have to provide both the username of the users and Twitter API credentials.
