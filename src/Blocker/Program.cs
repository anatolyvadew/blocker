﻿using System.Linq;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;

namespace TwitterBlocker
{
    class Program
    {
        private static TwitterClient _twitter;
        static async Task Main(string[] args)
        {
            _twitter = new TwitterClient(
                 new TwitterCredentials
                 {
                     ConsumerKey = "API Consumer Key",
                     ConsumerSecret = "API Consumer Secret",
                     AccessToken = "Access Token",
                     AccessTokenSecret = "Access Token Secret"
                 }
            );

            var names = new string[] { "" };
            var users = await _twitter.Users.GetUsersAsync(names);

            foreach(var user in users)
            {
                var followerIterator = user.GetFollowers();
                var followers = new IUser[] { };

                while (!followerIterator.Completed)
                {
                    var result = await followerIterator.NextPageAsync();
                    followers = result.Items; 

                    foreach (var follower in followers)
                         await follower.BlockUserAsync(); // Blocks the user
                }
            }

        }
    }

    static class Utils
    {
        private static string[] _keywords = new string[]
        {
            //pronouns
            "he/him", "she/her", "they/them",

            //emojis
            "☭", "🌹", "❤️", "🏳️‍⚧️",

            //mental illnesses
            "bipolar", "autis", "asperger",

            //acronyms & other
            "blm", "acab", "feminis", "k-pop"
        };

        public static bool IsMalicious(this IUser user) => _keywords.Any(user.Description.Contains);
    }

}
